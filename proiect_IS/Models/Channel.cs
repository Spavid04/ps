﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using proiect_IS.Utilities;

namespace proiect_IS.Models
{
    public class Channel
    {
        [Key]
        public int Id { get; set; }

        public Guid PublicId { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public Enumerations.ChannelVisibility Visibility { get; set; }
        public string Password { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreationTime { get; set; }
        
        public virtual List<Room> Rooms { get; set; }
        public virtual List<User> Users { get; set; }
        public virtual List<ChannelUserPrefs> ChannelUserPrefs { get; set; }
    }
}