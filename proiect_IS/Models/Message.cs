﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using proiect_IS.Utilities;

namespace proiect_IS.Models
{
    public class Message
    {
        [Key]
        public int Id { get; set; }

        public Guid PublicId { get; set; }

        public Enumerations.MessageType Type { get; set; }
        public string Contents { get; set; }
        public string Metadata { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreationTime { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime LastEditTime { get; set; }

        [Required]
        public virtual User User { get; set; }
        [Required]
        public virtual Room Room { get; set; }
    }
}