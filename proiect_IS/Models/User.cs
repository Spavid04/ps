﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using proiect_IS.Utilities;

namespace proiect_IS.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }

        public Guid PublicId { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
        public string Email { get; set; }

        public Enumerations.PublicUserLevel Level { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime FirstLoggedIn { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime LastLoggedIn { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime UsernameReservedTime { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime EmailSetTime { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime PasswordSetTime { get; set; }

        public virtual List<Channel> Channels { get; set; }
        public virtual List<ChannelUserPrefs> ChannelUserPrefs { get; set; }
        public virtual List<Message> Messages { get; set; }
    }
}