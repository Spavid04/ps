﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using proiect_IS.Utilities;

namespace proiect_IS.Models
{
    public class Room
    {
        [Key]
        public int Id { get; set; }

        public Guid PublicId { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public Enumerations.ChannelUserLevel MinimumRequiredUserLevel { get; set; }
        public Enumerations.MessageType AllowedMessageTypes { get; set; }
        
        public virtual List<Message> Messages { get; set; }
        [Required]
        public virtual Channel Channel { get; set; }
    }
}