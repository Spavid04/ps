﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using proiect_IS.Utilities;

namespace proiect_IS.Models
{
    public class Notification
    {
        public int Id { get; set; }

        public virtual User ForUser { get; set; }
        public virtual Message ForMessage { get; set; }
        
        public string Details { get; set; }

        public override string ToString()
        {
            if (ForMessage.Type == Enumerations.MessageType.Text)
                return ForMessage.Contents;
            else
                return ForMessage.Type.ToString();
        }
    }
}
