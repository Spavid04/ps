﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using proiect_IS.Utilities;

namespace proiect_IS.Models
{
    public class ChannelUserPrefs
    {
        [Key]
        public int Id { get; set; }
        
        public Guid ChannelId { get; set; }
        public Guid UserId { get; set; }

        public string Nickname { get; set; }
        public Enumerations.ChannelUserLevel Level { get; set; }

        [Required]
        public virtual User User { get; set; }
        [Required]
        public virtual Channel Channel { get; set; }
    }
}