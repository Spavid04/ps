﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using proiect_IS.Database;
using proiect_IS.Models;
using proiect_IS.Notifications;
using proiect_IS.Utilities;

namespace proiect_IS.Controllers
{
    public class NotificationController : Controller //todo change to ApiController to remove massive httprenderer overhead
    {
        private ChatDbContext DbContext = ConnectionFactory.GetContext("chatroom") as ChatDbContext;

        //public string json = null;

        public NotificationController(JsonToXml adapter) : base(adapter.Forward(), null, null)
        {
            
        }

        [HttpPost]
        public ActionResult @New(object @return, Message msg)
        {
            var user = DbContext.Users.First(x => x.PublicId == (Guid) Utils.GetUserCookie(Request.Cookies));

            lock (DbContext)
            {
                foreach (var chUser in msg.Room.Channel.Users)
                {
                    NotificationHandler.Subscribe(DbContext, new Notification()
                    {
                        Details = String.Concat(msg.Contents.Take(1000)),
                        ForMessage = msg,
                        ForUser = chUser,
                        Id = 0
                    });
                }
            }

            return (@return as Lazy<ActionResult>).Value;
        }

        [HttpPost]
        public void ReceievedCallback(int notificationId)
        {
            DbContext.Notifications.Remove(DbContext.Notifications.First(x => x.Id == notificationId));
            DbContext.SaveChanges();
        }
    }
}