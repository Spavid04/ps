﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Mvc;
using proiect_IS.Database;
using proiect_IS.Models;
using proiect_IS.Utilities;

namespace proiect_IS.Controllers
{
    public class ChatController : Controller
    {
        UserDAL userDal = new UserDAL();
        ChannelDAL channelDal = new ChannelDAL();
        RoomDAL roomDal = new RoomDAL();
        MessageDAL messageDal = new MessageDAL();
        ChannelUserPrefsDAL channelUserPrefsDal = new ChannelUserPrefsDAL();

        public ActionResult Home()
        {
            return RedirectToAction("Settings", "Chat");
        }

        #region User related actions

        public ActionResult Settings()
        {
            var user = userDal.GetUser((Guid)Utils.GetUserCookie(Request.Cookies));

            ViewBag.Page = "settings";

            return View(user);
        }

        [HttpPost]
        public ActionResult ChangePassword(string oldPassword, string newPassword)
        {
            var user = userDal.GetUser((Guid)Utils.GetUserCookie(Request.Cookies));

            if (user.Level == Enumerations.PublicUserLevel.Guest)
            {
                user = userDal.SetPassword(user.PublicId, newPassword);
                user = userDal.SetLevel(user.PublicId, Enumerations.PublicUserLevel.ReservedName);
            }
            else
            {
                if (!userDal.Verify(user.PublicId, oldPassword))
                {
                    ModelState.AddModelError("credential_error", "Invalid credentials specified!");
                }
                else
                {
                    user = userDal.SetPassword(user.PublicId, newPassword);
                }
            }

            return RedirectToAction("Settings", "Chat");
        }

        [HttpPost]
        public ActionResult ChangeName(string username, string password)
        {
            var user = userDal.GetUser((Guid)Utils.GetUserCookie(Request.Cookies));

            if (user.Level == Enumerations.PublicUserLevel.Guest)
            {
                user = userDal.SetName(user.PublicId, username);
            }
            else
            {
                if (!userDal.Verify(user.PublicId, password))
                {
                    ModelState.AddModelError("credential_error", "Invalid credentials specified!");
                }
                else
                {
                    user = userDal.SetName(user.PublicId, username);
                }
            }

            return RedirectToAction("Settings", "Chat");
        }

        [HttpPost]
        public ActionResult SetEmail(string email, string password)
        {
            var user = userDal.GetUser((Guid)Utils.GetUserCookie(Request.Cookies));

            if (!userDal.Verify(user.PublicId, password))
            {
                ModelState.AddModelError("credential_error", "Invalid credentials specified!");
            }
            else
            {
                user = userDal.SetEmail(user.PublicId, email);
                user = userDal.SetLevel(user.PublicId, Enumerations.PublicUserLevel.ReservedNameWithEmail);
            }

            return RedirectToAction("Settings", "Chat");
        }

        #endregion

        #region General channel related actions

        public ActionResult CreateChannel()
        {
            var user = userDal.GetUser((Guid)Utils.GetUserCookie(Request.Cookies));

            return View(user);
        }

        [HttpPost]
        public ActionResult CreateChannel(string name, string description, string visibility)
        {
            Enum.TryParse(visibility, true, out Enumerations.ChannelVisibility v);

            var user = userDal.GetUser((Guid)Utils.GetUserCookie(Request.Cookies));

            var channel = channelDal.CreateChannel(user.PublicId, name, description, v);

            return RedirectToAction("ChannelSettings", "Chat", new {id = channel.PublicId});
        }

        public ActionResult FindChannel()
        {
            return View((object)null);
        }

        [HttpPost]
        public ActionResult FindChannel(string search)
        {
            var user = userDal.GetUser((Guid)Utils.GetUserCookie(Request.Cookies));

            var channels = channelDal.SearchWithRegex(search).Except(user.Channels).ToList();

            return View(channels);
        }

        public ActionResult JoinChannel(string id)
        {
            Guid channelId = Guid.Parse(id);
            var user = userDal.GetUser((Guid)Utils.GetUserCookie(Request.Cookies));
            
            channelDal.JoinUser(channelId, user.PublicId);
            if (user.Level > Enumerations.PublicUserLevel.Guest)
            {
                channelUserPrefsDal.SetPrivilege(channelId, user.PublicId, Enumerations.ChannelUserLevel.User);
            }

            return RedirectToAction("ChannelSettings", "Chat", new { id = id });
        }

        public ActionResult LeaveChannel(string id)
        {
            Guid channelId = Guid.Parse(id);

            var user = userDal.GetUser((Guid)Utils.GetUserCookie(Request.Cookies));
            var channel = channelDal.GetChannel(channelId);
            var prefs = channel.ChannelUserPrefs;

            if (prefs.First(x => x.UserId == user.PublicId).Level == Enumerations.ChannelUserLevel.Owner &&
                prefs.Count(x => x.Level == Enumerations.ChannelUserLevel.Owner) == 1)
            {
                channelDal.RemoveChannel(channelId);
            }
            else
            {
                channelDal.KickUser(channelId, user.PublicId);
            }

            return RedirectToAction("Settings", "Chat");
        }

        #endregion

        #region Channel settings related actions

        public ActionResult ChannelSettings(string id)
        {
            Guid channelId = Guid.Parse(id);

            var user = userDal.GetUser((Guid)Utils.GetUserCookie(Request.Cookies));
            var channel = channelDal.GetChannel(channelId);

            ViewBag.Me = user;

            return View(channel);
        }

        [HttpPost]
        public ActionResult ChannelSettingsDesc(Guid channelId, string description)
        {
            channelDal.SetDescription(channelId, description);

            return RedirectToAction("ChannelSettings", "Chat", new { id = channelId });
        }

        [HttpPost]
        public ActionResult ChannelSettingsVis(Guid channelId, string visibility)
        {
            Enum.TryParse(visibility, true, out Enumerations.ChannelVisibility v);

            channelDal.SetVisibility(channelId, v);
            if (v == Enumerations.ChannelVisibility.Public)
            {
                channelDal.SetPassword(channelId, null);
            }

            return RedirectToAction("ChannelSettings", "Chat", new { id = channelId });
        }

        [HttpPost]
        public ActionResult ChannelSettingsNick(Guid channelId, string nickname)
        {
            var user = userDal.GetUser((Guid)Utils.GetUserCookie(Request.Cookies));

            channelUserPrefsDal.SetNickname(channelId, user.PublicId, nickname);

            return RedirectToAction("ChannelSettings", "Chat", new { id = channelId });
        }

        [HttpPost]
        public ActionResult ChannelSettingsPass(Guid channelId, string password)
        {
            channelDal.SetPassword(channelId, password);

            return RedirectToAction("ChannelSettings", "Chat", new { id = channelId });
        }

        [HttpPost]
        public ActionResult ChannelSettingsUser(Guid channelId, Guid userId, string action)
        {
            if (action == "kick")
            {
                //kick
                channelDal.KickUser(channelId, userId);
            }
            else
            {
                //change level
                Enum.TryParse(action, true, out Enumerations.ChannelUserLevel level);
                channelUserPrefsDal.SetPrivilege(channelId, userId, level);
            }

            return RedirectToAction("ChannelSettings", "Chat", new { id = channelId });
        }

        #endregion

        #region Room related actions

        public ActionResult CreateRoom(string id)
        {
            Guid channelId = Guid.Parse(id);
            
            var user = userDal.GetUser((Guid)Utils.GetUserCookie(Request.Cookies));
            var channel = channelDal.GetChannel(channelId);

            ViewBag.Me = user;

            return View(channel);
        }

        [HttpPost]
        public ActionResult CreateRoom(Guid channelId, string name, string description, string minimumUserLevel, bool text, bool image, bool audio, bool video, bool file)
        {
            Enum.TryParse(minimumUserLevel, true, out Enumerations.ChannelUserLevel level);

            Enumerations.MessageType types = Enumerations.MessageType.Text;
            if (image)
            {
                types |= Enumerations.MessageType.Image;
            }
            if (audio)
            {
                types |= Enumerations.MessageType.Audio;
            }
            if (video)
            {
                types |= Enumerations.MessageType.Video;
            }
            if (file)
            {
                types |= Enumerations.MessageType.File;
            }

            var room = roomDal.CreateRoom(channelId, name, description, level, types);
            
            return RedirectToAction("RoomSettings", "Chat", new { id = room.PublicId });
        }

        public ActionResult RoomSettings(string id)
        {
            Guid roomId = Guid.Parse(id);

            var user = userDal.GetUser((Guid)Utils.GetUserCookie(Request.Cookies));
            var room = roomDal.GetRoom(roomId);

            ViewBag.Me = user;

            return View(room);
        }

        [HttpPost]
        public ActionResult RoomSettingsName(Guid roomId, string name)
        {
            roomDal.SetName(roomId, name);

            return RedirectToAction("RoomSettings", "Chat", new { id = roomId });
        }

        [HttpPost]
        public ActionResult RoomSettingsDesc(Guid roomId, string description)
        {
            roomDal.SetDescription(roomId, description);

            return RedirectToAction("RoomSettings", "Chat", new { id = roomId });
        }

        [HttpPost]
        public ActionResult RoomSettingsLevel(Guid roomId, string level)
        {
            Enum.TryParse(level, true, out Enumerations.ChannelUserLevel userLevel);

            roomDal.SetMinimumUserLevel(roomId, userLevel);

            return RedirectToAction("RoomSettings", "Chat", new { id = roomId });
        }

        [HttpPost]
        public ActionResult RoomSettingsTypes(Guid roomId, bool text, bool image, bool audio, bool video, bool file)
        {
            Enumerations.MessageType types = Enumerations.MessageType.Text;
            if (image)
            {
                types |= Enumerations.MessageType.Image;
            }
            if (audio)
            {
                types |= Enumerations.MessageType.Audio;
            }
            if (video)
            {
                types |= Enumerations.MessageType.Video;
            }
            if (file)
            {
                types |= Enumerations.MessageType.File;
            }

            roomDal.SetAllowedMessageTypes(roomId, types);

            return RedirectToAction("RoomSettings", "Chat", new { id = roomId });
        }

        #endregion

        #region Message related actions

        public ActionResult ViewRoom(string id)
        {
            Guid roomId = Guid.Parse(id);

            var user = userDal.GetUser((Guid)Utils.GetUserCookie(Request.Cookies));
            var room = roomDal.GetRoom(roomId);

            ViewBag.Me = user;

            return View(room);
        }

        [HttpPost]
        public ActionResult PostMessage(Guid roomId, HttpPostedFileBase file, string uType, string content)
        {
            var user = userDal.GetUser((Guid)Utils.GetUserCookie(Request.Cookies));

            Enum.TryParse(uType, true, out Enumerations.MessageType type);

            Message msg = null;

            if (type == Enumerations.MessageType.Text)
            {
                messageDal.PostMessage(roomId, user.PublicId, type, content);
            }
            else
            {
                if (file.ContentLength > 0 && file.ContentLength < 1024 * 1024)
                {
                    byte[] data = new byte[file.ContentLength];

                    file.InputStream.Position = 0;
                    file.InputStream.Read(data, 0, file.ContentLength);

                    msg = messageDal.PostMessage(roomId, user.PublicId, type, Convert.ToBase64String(data), Path.GetFileName(file.FileName));
                }
            }

            //return RedirectToAction("New", "Notification",
            //    new {@return = new Lazy<RedirectToAction("ViewRoom", "Chat", new {id = roomId})>, message = msg});
            return RedirectToAction("ViewRoom", "Chat", new { id = roomId });
        }

        [HttpGet]
        public void GetFile(Guid id)
        {
            var message = messageDal.GetMessage(id);

            byte[] data = Convert.FromBase64String(message.Contents);

            Response.Clear();
            Response.ContentType = "application/force-download";
            Response.AddHeader("content-disposition", $"attachment; filename={message.Metadata}");
            Response.BinaryWrite(data);
            Response.End();
        }

        public ActionResult DeleteMessage(Guid id)
        {
            Guid roomId = messageDal.GetMessage(id).Room.PublicId;

            messageDal.RemoveMessage(id);

            return RedirectToAction("ViewRoom", "Chat", new { id = roomId });
        }

        #endregion
    }
}