﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using proiect_IS.Database;
using proiect_IS.Models;
using proiect_IS.Utilities;

namespace proiect_IS.Controllers
{
    public class UserController : Controller
    {
        UserDAL userDal = new UserDAL();

        public ActionResult Login()
        {
#if DEBUG
            ChatDbContext context = new ChatDbContext();

            context.Channels.RemoveRange(context.Channels);
            context.Rooms.RemoveRange(context.Rooms);
            context.ChannelUserPrefs.RemoveRange(context.ChannelUserPrefs);
            context.Messages.RemoveRange(context.Messages);
            context.Users.RemoveRange(context.Users);

            context.SaveChanges();
#endif

            ViewBag.HaltLayout = true;
            return View();
        }

        [HttpPost]
        public ActionResult Login(string name, string password)
        {
            if (String.IsNullOrEmpty(name))
            {
                ModelState.AddModelError("credential_error", "No username specified!");
                return View();
            }

            var user = userDal.GetUser(name);
            if (user == null)
            {
                //non-reserved name

                user = userDal.RegisterAnonymous(name);
                if (!String.IsNullOrEmpty(password))
                {
                    //also set password
                    user = userDal.SetPassword(user.PublicId, password);
                }
            }
            else
            {
                //reserved name => verify password

                if (!userDal.Verify(user.PublicId, password))
                {
                    ModelState.AddModelError("credential_error", "Invalid credentials specified!");
                    return View();
                }
            }

            Response.Cookies.Add(Utils.CreateUserCookie(user.PublicId, Utils.GetUsernameExpiryFromLevel(user.Level)));

            return RedirectToAction("Home", "Chat");
        }

        public ActionResult Logout()
        {
            var user = userDal.GetUser((Guid)Utils.GetUserCookie(Request.Cookies));

            if (user.Level == Enumerations.PublicUserLevel.Guest)
            {
                //delete the account

                userDal.DeleteUser(user.PublicId);
            }

            Response.Cookies["user"].Expires = DateTime.UtcNow.AddDays(-1);

            return RedirectToAction("Login", "User");
        }

        public ActionResult GenLayout()
        {
            var user = userDal.GetUser((Guid)Utils.GetUserCookie(Request.Cookies));

            return View(new LayoutModel()
            {
                CurrentUser = user
            });
        }
    }
}
