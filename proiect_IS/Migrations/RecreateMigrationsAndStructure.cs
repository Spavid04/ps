namespace proiect_IS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RecreateMigrationsAndStructure : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Channels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PublicId = c.Guid(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        Visibility = c.Int(nullable: false),
                        Password = c.String(),
                        CreationTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Rooms",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PublicId = c.Guid(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        MinimumRequiredUserLevel = c.Int(nullable: false),
                        AllowedMessageTypes = c.Int(nullable: false),
                        Channel_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Channels", t => t.Channel_Id)
                .Index(t => t.Channel_Id);
            
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PublicId = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                        Contents = c.String(),
                        CreationTime = c.DateTime(nullable: false),
                        LastEditTime = c.DateTime(nullable: false),
                        Room_Id = c.Int(),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Rooms", t => t.Room_Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.Room_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PublicId = c.Guid(nullable: false),
                        Username = c.String(),
                        Password = c.String(),
                        Email = c.String(),
                        Level = c.Int(nullable: false),
                        FirstLoggedIn = c.DateTime(nullable: false),
                        LastLoggedIn = c.DateTime(nullable: false),
                        UsernameReservedTime = c.DateTime(nullable: false),
                        EmailSetTime = c.DateTime(nullable: false),
                        Room_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Rooms", t => t.Room_Id)
                .Index(t => t.Room_Id);
            
            CreateTable(
                "dbo.ChannelUserPrefs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ChannelId = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                        Nickname = c.String(),
                        Level = c.Int(nullable: false),
                        Channel_Id = c.Int(),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Channels", t => t.Channel_Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.Channel_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.UserChannels",
                c => new
                    {
                        User_Id = c.Int(nullable: false),
                        Channel_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_Id, t.Channel_Id })
                .ForeignKey("dbo.Users", t => t.User_Id, cascadeDelete: true)
                .ForeignKey("dbo.Channels", t => t.Channel_Id, cascadeDelete: true)
                .Index(t => t.User_Id)
                .Index(t => t.Channel_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ChannelUserPrefs", "User_Id", "dbo.Users");
            DropForeignKey("dbo.ChannelUserPrefs", "Channel_Id", "dbo.Channels");
            DropForeignKey("dbo.Users", "Room_Id", "dbo.Rooms");
            DropForeignKey("dbo.Messages", "User_Id", "dbo.Users");
            DropForeignKey("dbo.UserChannels", "Channel_Id", "dbo.Channels");
            DropForeignKey("dbo.UserChannels", "User_Id", "dbo.Users");
            DropForeignKey("dbo.Messages", "Room_Id", "dbo.Rooms");
            DropForeignKey("dbo.Rooms", "Channel_Id", "dbo.Channels");
            DropIndex("dbo.UserChannels", new[] { "Channel_Id" });
            DropIndex("dbo.UserChannels", new[] { "User_Id" });
            DropIndex("dbo.ChannelUserPrefs", new[] { "User_Id" });
            DropIndex("dbo.ChannelUserPrefs", new[] { "Channel_Id" });
            DropIndex("dbo.Users", new[] { "Room_Id" });
            DropIndex("dbo.Messages", new[] { "User_Id" });
            DropIndex("dbo.Messages", new[] { "Room_Id" });
            DropIndex("dbo.Rooms", new[] { "Channel_Id" });
            DropTable("dbo.UserChannels");
            DropTable("dbo.ChannelUserPrefs");
            DropTable("dbo.Users");
            DropTable("dbo.Messages");
            DropTable("dbo.Rooms");
            DropTable("dbo.Channels");
        }
    }
}
