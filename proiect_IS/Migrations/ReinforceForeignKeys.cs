namespace proiect_IS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReinforceForeignKeys : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ChannelUserPrefs", "Channel_Id", "dbo.Channels");
            DropForeignKey("dbo.Rooms", "Channel_Id", "dbo.Channels");
            DropForeignKey("dbo.ChannelUserPrefs", "User_Id", "dbo.Users");
            DropForeignKey("dbo.Messages", "User_Id", "dbo.Users");
            DropForeignKey("dbo.Messages", "Room_Id", "dbo.Rooms");
            DropIndex("dbo.ChannelUserPrefs", new[] { "Channel_Id" });
            DropIndex("dbo.ChannelUserPrefs", new[] { "User_Id" });
            DropIndex("dbo.Messages", new[] { "Room_Id" });
            DropIndex("dbo.Messages", new[] { "User_Id" });
            DropIndex("dbo.Rooms", new[] { "Channel_Id" });
            AlterColumn("dbo.ChannelUserPrefs", "Channel_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.ChannelUserPrefs", "User_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Messages", "Room_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Messages", "User_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Rooms", "Channel_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.ChannelUserPrefs", "Channel_Id");
            CreateIndex("dbo.ChannelUserPrefs", "User_Id");
            CreateIndex("dbo.Messages", "Room_Id");
            CreateIndex("dbo.Messages", "User_Id");
            CreateIndex("dbo.Rooms", "Channel_Id");
            AddForeignKey("dbo.ChannelUserPrefs", "Channel_Id", "dbo.Channels", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Rooms", "Channel_Id", "dbo.Channels", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ChannelUserPrefs", "User_Id", "dbo.Users", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Messages", "User_Id", "dbo.Users", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Messages", "Room_Id", "dbo.Rooms", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Messages", "Room_Id", "dbo.Rooms");
            DropForeignKey("dbo.Messages", "User_Id", "dbo.Users");
            DropForeignKey("dbo.ChannelUserPrefs", "User_Id", "dbo.Users");
            DropForeignKey("dbo.Rooms", "Channel_Id", "dbo.Channels");
            DropForeignKey("dbo.ChannelUserPrefs", "Channel_Id", "dbo.Channels");
            DropIndex("dbo.Rooms", new[] { "Channel_Id" });
            DropIndex("dbo.Messages", new[] { "User_Id" });
            DropIndex("dbo.Messages", new[] { "Room_Id" });
            DropIndex("dbo.ChannelUserPrefs", new[] { "User_Id" });
            DropIndex("dbo.ChannelUserPrefs", new[] { "Channel_Id" });
            AlterColumn("dbo.Rooms", "Channel_Id", c => c.Int());
            AlterColumn("dbo.Messages", "User_Id", c => c.Int());
            AlterColumn("dbo.Messages", "Room_Id", c => c.Int());
            AlterColumn("dbo.ChannelUserPrefs", "User_Id", c => c.Int());
            AlterColumn("dbo.ChannelUserPrefs", "Channel_Id", c => c.Int());
            CreateIndex("dbo.Rooms", "Channel_Id");
            CreateIndex("dbo.Messages", "User_Id");
            CreateIndex("dbo.Messages", "Room_Id");
            CreateIndex("dbo.ChannelUserPrefs", "User_Id");
            CreateIndex("dbo.ChannelUserPrefs", "Channel_Id");
            AddForeignKey("dbo.Messages", "Room_Id", "dbo.Rooms", "Id");
            AddForeignKey("dbo.Messages", "User_Id", "dbo.Users", "Id");
            AddForeignKey("dbo.ChannelUserPrefs", "User_Id", "dbo.Users", "Id");
            AddForeignKey("dbo.Rooms", "Channel_Id", "dbo.Channels", "Id");
            AddForeignKey("dbo.ChannelUserPrefs", "Channel_Id", "dbo.Channels", "Id");
        }
    }
}
