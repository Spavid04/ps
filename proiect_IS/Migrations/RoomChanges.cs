namespace proiect_IS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RoomChanges : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Users", "Room_Id", "dbo.Rooms");
            DropIndex("dbo.Users", new[] { "Room_Id" });
            DropColumn("dbo.Users", "Room_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "Room_Id", c => c.Int());
            CreateIndex("dbo.Users", "Room_Id");
            AddForeignKey("dbo.Users", "Room_Id", "dbo.Rooms", "Id");
        }
    }
}
