namespace proiect_IS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DatetimeChanges : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Channels", "CreationTime", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Messages", "CreationTime", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Messages", "LastEditTime", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Users", "FirstLoggedIn", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Users", "LastLoggedIn", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Users", "UsernameReservedTime", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Users", "EmailSetTime", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Users", "PasswordSetTime", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "PasswordSetTime", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Users", "EmailSetTime", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Users", "UsernameReservedTime", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Users", "LastLoggedIn", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Users", "FirstLoggedIn", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Messages", "LastEditTime", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Messages", "CreationTime", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Channels", "CreationTime", c => c.DateTime(nullable: false));
        }
    }
}
