namespace proiect_IS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserModelChanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "PasswordSetTime", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "PasswordSetTime");
        }
    }
}
