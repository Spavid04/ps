namespace proiect_IS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MessageChanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Messages", "Metadata", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Messages", "Metadata");
        }
    }
}
