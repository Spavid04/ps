﻿using System;
using System.Web;
using proiect_IS.Models;

namespace proiect_IS.Utilities
{
    public static class Utils
    {
        public static string HashPassword(string password)
        {
            //todo hash it
            return password;
        }

        public static TimeSpan GetUsernameExpiryFromLevel(Enumerations.PublicUserLevel level)
        {
            switch (level)
            {
                case Enumerations.PublicUserLevel.Guest:
                    return TimeSpan.FromDays(1);
                case Enumerations.PublicUserLevel.ReservedName:
                    return TimeSpan.FromDays(7);
                case Enumerations.PublicUserLevel.ReservedNameWithEmail:
                    return TimeSpan.FromDays(31);
                default:
                    return TimeSpan.FromDays(1);
            }
        }

        public static Guid? GetUserCookie(HttpCookieCollection cookies)
        {
            if (cookies["user"] == null)
            {
                return null;
            }

            return Guid.Parse(cookies["user"].Value);
        }

        public static HttpCookie CreateUserCookie(Guid id, TimeSpan duration)
        {
            var cookie = new HttpCookie("user");

            cookie.Value = id.ToString();
            cookie.Expires = DateTime.UtcNow + duration;

            return cookie;
        }
    }
}