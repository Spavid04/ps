﻿using System;

namespace proiect_IS.Utilities
{
    public static class Enumerations
    {
        public enum PublicUserLevel
        {
            Guest = 0,
            ReservedName = 1,
            ReservedNameWithEmail = 2
        }

        public enum ChannelUserLevel
        {
            Guest = 0,
            User = 1,
            Moderator = 2,
            Administrator = 3,
            Owner = 4
        }

        public enum ChannelVisibility
        {
            Public = 0,
            Hidden = 1,
            Private = 2
        }

        [Flags]
        public enum MessageType
        {
            Text = 1,
            Image = 2,
            Audio = 4,
            Video = 8,
            File = 16
        }

        public enum PageType
        {
            Reserved = 0,
            Channel = 1,
            Room = 2
        }
    }
}