﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace proiect_IS.Database
{
    public static class ConnectionFactory
    {
        public static DbContext GetContext(string @for)
        {
            switch (@for)
            {
                case "chatroom":
                    return new ChatDbContext();
            }

            return null;
        }
    }
}