﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using proiect_IS.Models;
using proiect_IS.Utilities;

namespace proiect_IS.Database
{
    public class RoomDAL
    {
        public ChatDbContext DbContext = ConnectionFactory.GetContext("chatroom") as ChatDbContext;

        /// <summary>
        /// Saves all changes made.
        /// </summary>
        public void Save()
        {
            DbContext.SaveChanges();
        }

        /// <summary>
        /// Creates a new room in the specified channel.
        /// </summary>
        /// <param name="channelId">channel id</param>
        /// <param name="name">room name</param>
        /// <param name="description">room description</param>
        /// <param name="minimumUserLevel">room minimum user level requirement</param>
        /// <param name="allowedMessageTypes">room message types allowed</param>
        /// <returns></returns>
        public Room CreateRoom(Guid channelId, string name, string description,
            Enumerations.ChannelUserLevel minimumUserLevel, params Enumerations.MessageType[] allowedMessageTypes)
        {
            Channel c = DbContext.Channels.First(x => x.PublicId == channelId);

            Enumerations.MessageType calculatedTypes = Enumerations.MessageType.Text;

            foreach (var allowedMessageType in allowedMessageTypes)
            {
                calculatedTypes |= allowedMessageType;
            }

            Room room = new Room()
            {
                Channel = c,
                Description = description,
                Messages = new List<Message>(),
                PublicId = Guid.NewGuid(),
                MinimumRequiredUserLevel = minimumUserLevel,
                Name = name,
                AllowedMessageTypes = calculatedTypes
            };

            room = DbContext.Rooms.Add(room);
            this.Save();

            return room;
        }

        /// <summary>
        /// Sets the name of a room.
        /// </summary>
        /// <param name="roomId">room id</param>
        /// <param name="name">desired name</param>
        /// <returns></returns>
        public Room SetName(Guid roomId, string name)
        {
            Room r = DbContext.Rooms.First(x => x.PublicId == roomId);

            r.Name = name;
            this.Save();

            return r;
        }

        /// <summary>
        /// Sets the description of a room.
        /// </summary>
        /// <param name="roomId">room id</param>
        /// <param name="description">desired description</param>
        /// <returns></returns>
        public Room SetDescription(Guid roomId, string description)
        {
            Room r = DbContext.Rooms.First(x => x.PublicId == roomId);

            r.Description = description;
            this.Save();

            return r;
        }

        /// <summary>
        /// Sets the minimum required user level of a room.
        /// </summary>
        /// <param name="roomId">room id</param>
        /// <param name="level">desired level</param>
        /// <returns></returns>
        public Room SetMinimumUserLevel(Guid roomId, Enumerations.ChannelUserLevel level)
        {
            Room r = DbContext.Rooms.First(x => x.PublicId == roomId);

            r.MinimumRequiredUserLevel = level;
            this.Save();

            return r;
        }

        /// <summary>
        /// Sets the allowed message types of a room.
        /// </summary>
        /// <param name="roomId">room id</param>
        /// <param name="allowedMessageTypes">desired types</param>
        /// <returns></returns>
        public Room SetAllowedMessageTypes(Guid roomId, params Enumerations.MessageType[] allowedMessageTypes)
        {
            Room r = DbContext.Rooms.First(x => x.PublicId == roomId);

            Enumerations.MessageType calculatedTypes = Enumerations.MessageType.Text;

            foreach (var allowedMessageType in allowedMessageTypes)
            {
                calculatedTypes |= allowedMessageType;
            }

            r.AllowedMessageTypes = calculatedTypes;
            this.Save();

            return r;
        }

        /// <summary>
        /// Gets a room by it's public id.
        /// </summary>
        /// <param name="roomId">room id</param>
        /// <returns></returns>
        public Room GetRoom(Guid roomId)
        {
            Room r = DbContext.Rooms.FirstOrDefault(x => x.PublicId == roomId);

            return r;
        }
    }
}