﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using proiect_IS.Models;
using proiect_IS.Utilities;

namespace proiect_IS.Database
{
    public class ChannelDAL
    {
        public ChatDbContext DbContext = ConnectionFactory.GetContext("chatroom") as ChatDbContext;

        /// <summary>
        /// Saves all changes made.
        /// </summary>
        public void Save()
        {
            DbContext.SaveChanges();
        }

        /// <summary>
        /// Creates a new channel with the specified parameters.
        /// </summary>
        /// <param name="creatorId">owner of the channel</param>
        /// <param name="name">public channel name</param>
        /// <param name="description">optional description</param>
        /// <param name="visibility">channel visibility</param>
        /// <returns>the channel instance</returns>
        public Channel CreateChannel(Guid creatorId, string name, string description,
            Enumerations.ChannelVisibility visibility)
        {
            User u = DbContext.Users.First(x => x.PublicId == creatorId);

            Channel c = new Channel()
            {
                PublicId = Guid.NewGuid(),
                CreationTime = DateTime.UtcNow,
                Description = description,
                Name = name,
                Password = null,
                Rooms = new List<Room>(),
                Users = new List<User>() {u},
                Visibility = visibility
            };

            c = DbContext.Channels.Add(c);
            this.Save();

            ChannelUserPrefsDAL channelUserPrefsDal = new ChannelUserPrefsDAL();
            channelUserPrefsDal.NewUser(c.PublicId, u.PublicId, Enumerations.ChannelUserLevel.Owner);

            return c;
        }

        /// <summary>
        /// Sets the name of a channel.
        /// </summary>
        /// <param name="id">the channel id</param>
        /// <param name="name">the name</param>
        /// <returns></returns>
        public Channel SetName(Guid id, string name)
        {
            Channel c = DbContext.Channels.First(x => x.PublicId == id);

            c.Name = name;
            this.Save();

            return c;
        }

        /// <summary>
        /// Sets the description of a channel.
        /// </summary>
        /// <param name="id">the channel id</param>
        /// <param name="description">the description</param>
        /// <returns></returns>
        public Channel SetDescription(Guid id, string description)
        {
            Channel c = DbContext.Channels.First(x => x.PublicId == id);

            c.Description = description;
            this.Save();

            return c;
        }

        /// <summary>
        /// Sets the visibility of a channel.
        /// </summary>
        /// <param name="id">the channel id</param>
        /// <param name="visibility">the visibility</param>
        /// <returns></returns>
        public Channel SetVisibility(Guid id, Enumerations.ChannelVisibility visibility)
        {
            Channel c = DbContext.Channels.First(x => x.PublicId == id);

            c.Visibility = visibility;
            this.Save();

            return c;
        }

        /// <summary>
        /// Sets the password of a channel.
        /// </summary>
        /// <param name="id">the channel id</param>
        /// <param name="password">the password</param>
        /// <returns></returns>
        public Channel SetPassword(Guid id, string password)
        {
            Channel c = DbContext.Channels.First(x => x.PublicId == id);

            c.Password = Utils.HashPassword(password);
            this.Save();

            return c;
        }

        /// <summary>
        /// Gets a channel by it's public id.
        /// </summary>
        /// <param name="id">channel id</param>
        /// <returns></returns>
        public Channel GetChannel(Guid id)
        {
            Channel c = DbContext.Channels.FirstOrDefault(x => x.PublicId == id);

            return c;
        }

        /// <summary>
        /// Removes a user from a channel.
        /// Also removes preferences for that channel-user pair.
        /// </summary>
        /// <param name="id">channel id</param>
        /// <param name="userId">user id</param>
        /// <returns></returns>
        public Channel KickUser(Guid id, Guid userId)
        {
            Channel c = DbContext.Channels.First(x => x.PublicId == id);

            var user = c.Users.First(x => x.PublicId == userId);
            c.Users.Remove(user);

            var prefs = DbContext.ChannelUserPrefs.First(x => x.UserId == userId && x.ChannelId == id);
            DbContext.ChannelUserPrefs.Remove(prefs);

            this.Save();

            return c;
        }

        /// <summary>
        /// Removes a channel from the system.
        /// Also removes preferences for all users in the channel.
        /// </summary>
        /// <param name="id">channel id</param>
        public void RemoveChannel(Guid id)
        {
            Channel c = DbContext.Channels.First(x => x.PublicId == id);

            DbContext.ChannelUserPrefs.RemoveRange(DbContext.ChannelUserPrefs.Where(x => x.ChannelId == id));

            DbContext.Channels.Remove(c);

            this.Save();
        }

        /// <summary>
        /// Searches all the channels for the ones whose name match the supplied regex.
        /// </summary>
        /// <param name="search">regex pattern</param>
        /// <returns>channel list</returns>
        public List<Channel> SearchWithRegex(string search)
        {
            return DbContext.Channels.Where(x => x.Visibility == Enumerations.ChannelVisibility.Public).ToList()
                .Where(x => Regex.IsMatch(x.Name, search)).ToList();
        }

        /// <summary>
        /// Adds a user to the specified channel.
        /// </summary>
        /// <param name="channelId">channel id</param>
        /// <param name="userId">user id</param>
        public void JoinUser(Guid channelId, Guid userId)
        {
            Channel c = DbContext.Channels.First(x => x.PublicId == channelId);

            var u = DbContext.Users.First(x => x.PublicId == userId);

            c.Users.Add(u);
            this.Save();

            ChannelUserPrefsDAL channelUserPrefsDal = new ChannelUserPrefsDAL();
            channelUserPrefsDal.NewUser(c.PublicId, u.PublicId, Enumerations.ChannelUserLevel.Guest);
        }
    }
}