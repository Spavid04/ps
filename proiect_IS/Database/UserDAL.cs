﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using proiect_IS.Models;
using proiect_IS.Utilities;

namespace proiect_IS.Database
{
    public class UserDAL
    {
        public ChatDbContext DbContext = ConnectionFactory.GetContext("chatroom") as ChatDbContext;

        /// <summary>
        /// Saves all changes made.
        /// </summary>
        public void Save()
        {
            DbContext.SaveChanges();
        }

        /// <summary>
        /// Checks if the supplied credentials are valid.
        /// </summary>
        /// <param name="userId">the user id</param>
        /// <param name="password">the unhashed password</param>
        /// <returns>whether the verification succeded</returns>
        public bool Verify(Guid userId, string password)
        {
            var user = DbContext.Users.FirstOrDefault(x => x.PublicId == userId);

            if (user == null)
            {
                return false;
            }

            if (Utils.HashPassword(password) != user.Password)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Register a username as an anonymous identity.
        /// </summary>
        /// <param name="username">the username</param>
        /// <returns>the user instance</returns>
        public User RegisterAnonymous(string username)
        {
            var user = new User()
            {
                Email = null,
                EmailSetTime = DateTime.MinValue,
                FirstLoggedIn = DateTime.UtcNow,
                PublicId = Guid.NewGuid(),
                Channels = new List<Channel>(),
                ChannelUserPrefs = new List<ChannelUserPrefs>(),
                Username = username,
                LastLoggedIn = DateTime.UtcNow,
                Level = Enumerations.PublicUserLevel.Guest,
                Messages = new List<Message>(),
                Password = null,
                UsernameReservedTime = DateTime.MinValue,
                PasswordSetTime = DateTime.MinValue
            };

            user = DbContext.Users.Add(user);

            this.Save();

            return user;
        }

        /// <summary>
        /// Sets a password for the specified user.
        /// Also promotes the user in any channel joined as a guest.
        /// </summary>
        /// <param name="userId">the user id</param>
        /// <param name="password">the desired password</param>
        /// <returns>the user instance</returns>
        public User SetPassword(Guid userId, string password)
        {
            var user = DbContext.Users.First(x => x.PublicId == userId);

            user.Password = Utils.HashPassword(password);
            user.PasswordSetTime = DateTime.UtcNow;

            this.Save();

            foreach (var userChannelUserPref in user.ChannelUserPrefs)
            {
                if (userChannelUserPref.Level == Enumerations.ChannelUserLevel.Guest)
                    userChannelUserPref.Level = Enumerations.ChannelUserLevel.User;
            }
            this.Save();

            return user;
        }

        /// <summary>
        /// Sets an email for the specified user.
        /// </summary>
        /// <param name="userId">the user id</param>
        /// <param name="email">the desired email</param>
        /// <returns>the user instance</returns>
        public User SetEmail(Guid userId, string email)
        {
            var user = DbContext.Users.First(x => x.PublicId == userId);

            user.Email = email;
            user.EmailSetTime = DateTime.UtcNow;

            this.Save();

            return user;
        }

        /// <summary>
        /// Changes a name for the specified user.
        /// </summary>
        /// <param name="userId">the user id</param>
        /// <param name="username">the desired name</param>
        /// <returns>the user instance</returns>
        public User SetName(Guid userId, string username)
        {
            var user = DbContext.Users.First(x => x.PublicId == userId);

            user.Username = username;
            user.UsernameReservedTime = DateTime.UtcNow;

            this.Save();

            return user;
        }

        /// <summary>
        /// Changes the user's public level.
        /// </summary>
        /// <param name="userId">user id</param>
        /// <param name="level">desired level</param>
        /// <returns>the user instance</returns>
        public User SetLevel(Guid userId, Enumerations.PublicUserLevel level)
        {
            var user = DbContext.Users.First(x => x.PublicId == userId);

            user.Level = level;

            this.Save();

            return user;
        }
        
        /// <summary>
        /// Fetches a user with the specified id.
        /// </summary>
        /// <param name="guid">the id</param>
        /// <returns>the user</returns>
        public User GetUser(Guid guid)
        {
            User user = DbContext.Users.FirstOrDefault(x => x.PublicId == guid);

            return user;
        }

        /// <summary>
        /// Fetches a user with the specified username.
        /// </summary>
        /// <param name="username">the username</param>
        /// <returns>the user</returns>
        public User GetUser(string username)
        {
            User user = DbContext.Users.FirstOrDefault(x => x.Username == username);

            return user;
        }

        /// <summary>
        /// Deletes a user from the system.
        /// </summary>
        /// <param name="userId">user id</param>
        public void DeleteUser(Guid userId)
        {
            User user = DbContext.Users.First(x => x.PublicId == userId);

            DbContext.Users.Remove(user);
            this.Save();
        }
    }
}