﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using proiect_IS.Models;
using proiect_IS.Utilities;

namespace proiect_IS.Database
{
    public class ChannelUserPrefsDAL
    {
        public ChatDbContext DbContext = ConnectionFactory.GetContext("chatroom") as ChatDbContext;

        /// <summary>
        /// Saves all changes made.
        /// </summary>
        public void Save()
        {
            DbContext.SaveChanges();
        }

        /// <summary>
        /// Creates a new preference setting with default values.
        /// </summary>
        /// <param name="channelId">channel id</param>
        /// <param name="userId">user id</param>
        /// <param name="level">optional user level</param>
        /// <returns></returns>
        public ChannelUserPrefs NewUser(Guid channelId, Guid userId, Enumerations.ChannelUserLevel level = Enumerations.ChannelUserLevel.Guest)
        {
            Channel c = DbContext.Channels.First(x => x.PublicId == channelId);
            User u = DbContext.Users.First(x => x.PublicId == userId);

            ChannelUserPrefs cup = new ChannelUserPrefs()
            {
                Channel = c,
                UserId = userId,
                ChannelId = channelId,
                Level = level,
                Nickname = null,
                User = u
            };

            cup = DbContext.ChannelUserPrefs.Add(cup);
            this.Save();

            return cup;
        }

        /// <summary>
        /// Changes a user's channel privilege.
        /// </summary>
        /// <param name="channelId">channel id</param>
        /// <param name="userId">user id</param>
        /// <param name="level">desired level</param>
        /// <returns></returns>
        public ChannelUserPrefs SetPrivilege(Guid channelId, Guid userId, Enumerations.ChannelUserLevel level)
        {
            Channel c = DbContext.Channels.First(x => x.PublicId == channelId);
            User u = DbContext.Users.First(x => x.PublicId == userId);
            ChannelUserPrefs cup =
                DbContext.ChannelUserPrefs.First(x => x.ChannelId == channelId && x.UserId == userId);

            cup.Level = level;
            this.Save();

            return cup;
        }

        /// <summary>
        /// Changes a user's channel nickname
        /// </summary>
        /// <param name="channelId">channel id</param>
        /// <param name="userId">user id</param>
        /// <param name="nickname">desired nickname</param>
        /// <returns></returns>
        public ChannelUserPrefs SetNickname(Guid channelId, Guid userId, string nickname)
        {
            Channel c = DbContext.Channels.First(x => x.PublicId == channelId);
            User u = DbContext.Users.First(x => x.PublicId == userId);
            ChannelUserPrefs cup =
                DbContext.ChannelUserPrefs.First(x => x.ChannelId == channelId && x.UserId == userId);

            cup.Nickname = nickname;
            this.Save();

            return cup;
        }
    }
}