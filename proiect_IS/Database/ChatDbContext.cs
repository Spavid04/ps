﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using proiect_IS.Models;

namespace proiect_IS.Database
{
    public class ChatDbContext : DbContext
    {
        public const string ConnectionString = @"Data Source=(localdb)\ProjectsV13;Initial Catalog=chatroom;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        public ChatDbContext()
        {
            //this.Database.Connection.ConnectionString = ConnectionString;
            this.Database.Connection.ConnectionString =
                System.Configuration.ConfigurationManager.AppSettings["ConnectionString"];
        }

        public DbSet<Channel> Channels { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<ChannelUserPrefs> ChannelUserPrefs { get; set; }
        
        public DbSet<Notification> Notifications { get; set; }
    }
}