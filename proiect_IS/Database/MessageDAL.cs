﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using proiect_IS.Models;
using proiect_IS.Utilities;

namespace proiect_IS.Database
{
    public class MessageDAL
    {
        public ChatDbContext DbContext = ConnectionFactory.GetContext("chatroom") as ChatDbContext;

        /// <summary>
        /// Saves all changes made.
        /// </summary>
        public void Save()
        {
            DbContext.SaveChanges();
        }

        /// <summary>
        /// Posts a message in the specified room.
        /// </summary>
        /// <param name="roomId">room id</param>
        /// <param name="userId">user id</param>
        /// <param name="type">message type</param>
        /// <param name="contents">message contents</param>
        /// <returns></returns>
        public Message PostMessage(Guid roomId, Guid userId, Enumerations.MessageType type, string contents, string metadata = null)
        {
            Room r = DbContext.Rooms.First(x => x.PublicId == roomId);
            User u = DbContext.Users.First(x => x.PublicId == userId);

            Message message = new Message()
            {
                User = u,
                Room = r,
                Contents = contents,
                Metadata = metadata,
                PublicId = Guid.NewGuid(),
                CreationTime = DateTime.UtcNow,
                LastEditTime = DateTime.MinValue,
                Type = type
            };

            message = DbContext.Messages.Add(message);
            this.Save();

            return message;
        }

        /// <summary>
        /// Deletes a message from the system.
        /// </summary>
        /// <param name="messageId">message id</param>
        public void RemoveMessage(Guid messageId)
        {
            Message m = DbContext.Messages.First(x => x.PublicId == messageId);

            DbContext.Messages.Remove(m);

            this.Save();
        }

        /// <summary>
        /// Gets a message by it's public id.
        /// </summary>
        /// <param name="messageId">message id</param>
        /// <returns></returns>
        public Message GetMessage(Guid messageId)
        {
            Message m = DbContext.Messages.First(x => x.PublicId == messageId);

            return m;
        }
    }
}